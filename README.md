# README #

This is a simple app that trackes lates earthquakes and tweets connected with them. It require an internet connection to run at least for the first time. App is caching downloaded earthquakes informations in local database. Tweets are not stored, but the app is prepared for that with minimum coding. App is downloading latest earthquakes data in background data using android native syncing method. The sync is executed every 20 minutes. 

Clicking on earthquake row will take us to detailed page, where we also can load tweets from 25km range from epicenter. 

### What is it for? ###
This app can be used as a demonstration of few advanced android technics like:

* Using rest services to download data from internet

* Processing downloaded data efficently using components of AprilApps library

* Syncing data in background using SyncAdapter
* Using ORMLite for maintaning SQLite database

* Using twitter4j library to search for public tweets with specified location cryteria
* Maintaning screen rotation efficiently
* Using ActionBar and DrawerLayout efficiently

### Necessary setup ###

If you want just take a tour, the app should run just fine. It uses my Twitter and Google dev account however to connect to Twitter API and Google Maps API. It's important to sign the app with attached eqt.jks certificate in order to keep Google Maps API working. Fortunately Android Studio allows to configure build so, the included cert is used even in debug. Keystore credentials are availible in build.gradle of app module.

### Configuration ###
Constants.java interface is the place, where most of the configuration is stored.

Earthquake rows colors depends on magnitude type, as descrived here: http://seisan.ird.nc/USGS/mirror/neic.usgs.gov/neis/epic/code_magnitude.html


I was not sure, what colors should be used in which case, so they are pretty random. Coloring can be reconfigured in Earthquake.java inside getMagTypeColor() method.