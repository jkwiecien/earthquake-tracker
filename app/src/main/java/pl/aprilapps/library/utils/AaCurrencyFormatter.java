package pl.aprilapps.library.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.text.NumberFormat;

public class AaCurrencyFormatter {

	public static final int SYMBOL_ON_THE_LEFT = 0;
	public static final int SYMBOL_ON_THE_RIGHT = 1;

	public static final String KEY_SYMBOL_POSITION = "symbol_position_preference";
	public static final String KEY_SYMBOL = "symbol_preference";

	// public static final String DEFAULT_CURRENCY_SYMBOL = "&#8364;";
	public static final String DEFAULT_CURRENCY_SYMBOL = "$";

	private NumberFormat numberFormat;
	private Context context;

	public AaCurrencyFormatter(Context context) {
		this.context = context;
		this.numberFormat = NumberFormat.getInstance();
		this.numberFormat.setMinimumFractionDigits(2);
		this.numberFormat.setMaximumFractionDigits(2);
	}

	public String format(double number) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		int position = SYMBOL_ON_THE_LEFT;
		try {
			position = preferences.getInt(KEY_SYMBOL_POSITION, SYMBOL_ON_THE_LEFT);
		} catch (Exception e) {
			position = Integer.parseInt(preferences.getString(KEY_SYMBOL_POSITION, Integer.toString(SYMBOL_ON_THE_LEFT)));
		}

		switch (position) {
		case SYMBOL_ON_THE_LEFT:
			return symbolString + " " + numberString;
		case SYMBOL_ON_THE_RIGHT:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String format(float number) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		int position = SYMBOL_ON_THE_LEFT;
		try {
			position = preferences.getInt(KEY_SYMBOL_POSITION, SYMBOL_ON_THE_LEFT);
		} catch (Exception e) {
			position = Integer.parseInt(preferences.getString(KEY_SYMBOL_POSITION, Integer.toString(SYMBOL_ON_THE_LEFT)));
		}

		switch (position) {
		case SYMBOL_ON_THE_LEFT:
			return symbolString + " " + numberString;
		case SYMBOL_ON_THE_RIGHT:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String format(int number) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		int position = SYMBOL_ON_THE_LEFT;
		try {
			position = preferences.getInt(KEY_SYMBOL_POSITION, SYMBOL_ON_THE_LEFT);
		} catch (Exception e) {
			position = Integer.parseInt(preferences.getString(KEY_SYMBOL_POSITION, Integer.toString(SYMBOL_ON_THE_LEFT)));
		}

		switch (position) {
		case SYMBOL_ON_THE_LEFT:
			return symbolString + " " + numberString;
		case SYMBOL_ON_THE_RIGHT:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String getExchangeRate(float number) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(4);
		nf.setMaximumFractionDigits(4);

		String numberString = nf.format(number);
		return numberString;
	}

	public String getExchangeRate(double number) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(4);
		nf.setMaximumFractionDigits(4);

		String numberString = nf.format(number);
		return numberString;
	}

	public String getExchangeRateWithCurrency(float number) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(4);
		nf.setMaximumFractionDigits(4);
		String numberString = nf.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		int position = SYMBOL_ON_THE_LEFT;
		try {
			position = preferences.getInt(KEY_SYMBOL_POSITION, SYMBOL_ON_THE_LEFT);
		} catch (Exception e) {
			position = Integer.parseInt(preferences.getString(KEY_SYMBOL_POSITION, Integer.toString(SYMBOL_ON_THE_LEFT)));
		}

		switch (position) {
		case SYMBOL_ON_THE_LEFT:
			return symbolString + " " + numberString;
		case SYMBOL_ON_THE_RIGHT:
			return numberString + " " + symbolString;
		default:
			return symbolString + " " + numberString;
		}
	}

	public String format(double number, boolean addCurrencySymbol) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		String numberString = numberFormat.format(number);
		String symbolString = preferences.getString(KEY_SYMBOL, DEFAULT_CURRENCY_SYMBOL);

		int position = SYMBOL_ON_THE_LEFT;
		try {
			position = preferences.getInt(KEY_SYMBOL_POSITION, SYMBOL_ON_THE_LEFT);
		} catch (Exception e) {
			position = Integer.parseInt(preferences.getString(KEY_SYMBOL_POSITION, Integer.toString(SYMBOL_ON_THE_LEFT)));
		}

		if (addCurrencySymbol) {
			switch (position) {
			case SYMBOL_ON_THE_LEFT:
				return symbolString + " " + numberString;
			case SYMBOL_ON_THE_RIGHT:
				return numberString + " " + symbolString;
			default:
				return symbolString + " " + numberString;
			}
		} else {
			return numberString;
		}

	}

}
