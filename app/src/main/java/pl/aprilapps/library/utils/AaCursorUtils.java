package pl.aprilapps.library.utils;

import java.util.Calendar;
import java.util.Date;

import android.database.Cursor;

public class AaCursorUtils extends AaUtils {

	private Cursor cursor;

	public AaCursorUtils(Cursor cursor) {
		super();
		this.cursor = cursor;
	}

	// METODY STATYCZNE
	public static String getString(Cursor cursor, String columnName) {
		return cursor.getString(cursor.getColumnIndex(columnName));
	}

	public static int getInt(Cursor cursor, String columnName) {
		return cursor.getInt(cursor.getColumnIndex(columnName));
	}

	public static long getLong(Cursor cursor, String columnName) {
		return cursor.getLong(cursor.getColumnIndex(columnName));
	}

	public static float getFloat(Cursor cursor, String columnName) {
		return cursor.getFloat(cursor.getColumnIndex(columnName));
	}

	public static double getDouble(Cursor cursor, String columnName) {
		return cursor.getDouble(cursor.getColumnIndex(columnName));
	}

	public static boolean getBoolean(Cursor cursor, String columnName) {
		return cursor.getInt(cursor.getColumnIndex(columnName)) == 1;
	}

	public String getString(String columnName) {
		return cursor.getString(cursor.getColumnIndex(columnName));
	}

	public int getInt(String columnName) {
		return cursor.getInt(cursor.getColumnIndex(columnName));
	}

	public long getLong(String columnName) {
		return cursor.getLong(cursor.getColumnIndex(columnName));
	}

	public float getFloat(String columnName) {
		return cursor.getFloat(cursor.getColumnIndex(columnName));
	}

	public double getDouble(String columnName) {
		return cursor.getDouble(cursor.getColumnIndex(columnName));
	}

	public boolean getBoolean(String columnName) {
		return cursor.getInt(cursor.getColumnIndex(columnName)) == 1;
	}

	public byte[] getBlob(String columnName) {
		return cursor.getBlob(cursor.getColumnIndex(columnName));
	}

}
