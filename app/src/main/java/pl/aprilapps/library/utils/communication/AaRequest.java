package pl.aprilapps.library.utils.communication;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class AaRequest<T> {

	protected HashMap<String, Object> params;
	protected HashMap<String, String> headers;
	protected StringBuilder urlBuilder;
	protected URL url;
	protected HttpURLConnection connection;
	protected int statusCode;
	private Context context;

	public AaRequest(String functionName, Context context) throws MalformedURLException {
		this.context = context;
		urlBuilder = new StringBuilder();

        urlBuilder.append(getAPIUrl());
        if (functionName != null) {
            urlBuilder.append("/").append(functionName);
        }

		headers = new HashMap<String, String>();
		params = new HashMap<String, Object>();
	}

	/**
	 * @param key
	 * @param value
	 *            Wartosc parametru. Dopuszczalne obiekty to String , Long i Integer
	 */
	public void addParam(String key, Object value) {
		params.put(key, value);
	}

	public void addHeader(String key, String header) {
		headers.put(key, header);
	}

	public abstract void buildQuery() throws IOException, UnsupportedEncodingException;

	public abstract InputStream execute() throws Exception;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void injectHeders() {
		connection.setRequestProperty("User-Agent", android.os.Build.MODEL);
		Iterator it = headers.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pair = (Map.Entry) it.next();
			Object value = pair.getValue();

			String key = pair.getKey();
			String header = value.toString();

			connection.setRequestProperty(key, header);
		}
	}

	public int getStatusCode() {
		return statusCode;
	}

	public T getResponseObject() throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(execute()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		return parseResponse(response.toString());
	}

	public abstract T parseResponse(String responseString) throws Exception;

	public abstract String getAPIUrl();

	public boolean useSSL() {
		return url.toString().contains("https");
	}

	public Context getContext() {
		return context;
	}

	public int getTimeout() {
		return 30000;
	}
}
