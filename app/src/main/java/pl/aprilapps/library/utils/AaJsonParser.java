package pl.aprilapps.library.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AaJsonParser<T> {

	private Class<T> type;

	public AaJsonParser(Class<T> type) {
		this.setType(type);
	}

	public List<T> parseJSONArray(JSONArray jsonArray) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException,
			InvocationTargetException, JSONException {
		List<T> items = new ArrayList<T>();

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject object = jsonArray.getJSONObject(i);
			items.add(parseJSONObject(object));
		}

		return items;
	}

	public List<String> jsonStringArrayToList(JSONArray jsonArray) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException, JSONException {
		List<String> items = new ArrayList<String>();

		for (int i = 0; i < jsonArray.length(); i++) {
			items.add(jsonArray.getString(i));
		}

		return items;
	}

	public String[] jsonStringArrayToArray(JSONArray jsonArray) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException,
			IllegalAccessException, InvocationTargetException, JSONException {
		String[] items = new String[jsonArray.length()];
		for (int i = 0; i < jsonArray.length(); i++) {
			items[i] = jsonArray.getString(i);
		}
		return items;
	}

	public T parseJSONObject(JSONObject object) throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException,
			InvocationTargetException {

		Class<?> cl = getType();
		Constructor<?> constructor = cl.getConstructor(JSONObject.class);
		T item = (T) constructor.newInstance(object);

		return item;
	}

	public Class<T> getType() {
		return type;
	}

	public void setType(Class<T> type) {
		this.type = type;
	}



}
