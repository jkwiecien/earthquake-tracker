package pl.aprilapps.library.utils.communication;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public abstract class AaGetRequest<T> extends AaRequest<T> {

	public AaGetRequest(String functionName, Context context) throws MalformedURLException {
		super(functionName, context);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void buildQuery() throws IOException, UnsupportedEncodingException {
		if (params.size() > 0) {
			Iterator it = params.entrySet().iterator();
			int i = 0;
			while (it.hasNext()) {
				Map.Entry<String, Object> pair = (Map.Entry) it.next();
				Object value = pair.getValue();

				String param = value.toString();
				String key = pair.getKey();

				if (i == 0) {
					urlBuilder.append("?").append(key).append("=").append(URLEncoder.encode(param, "utf-8"));
				} else {
					urlBuilder.append("&").append(key).append("=").append(URLEncoder.encode(param, "utf-8"));
				}
				i++;
			}
		}
	}

	@Override
	public InputStream execute() throws Exception {
		buildQuery();
		url = new URL(urlBuilder.toString());
		if (useSSL()) {
			connection = (HttpsURLConnection) url.openConnection();
		} else {
			connection = (HttpURLConnection) url.openConnection();
		}
		connection.setConnectTimeout(getTimeout());
		injectHeders();
		statusCode = connection.getResponseCode();

		if (statusCode <= 200 && statusCode < 300) {
			InputStream responseData = connection.getInputStream();
			return responseData;
		} else {
			InputStream error = connection.getErrorStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(error, "UTF-8"));
			String line;
			Log.e("org.test", "error message received from server:");
			while ((line = reader.readLine()) != null) {
				Log.e("org.test", line);
			}
			throw new IOException(String.format("invalid response code: %d (%s)", connection.getResponseCode(), connection.getResponseMessage()));
		}
	}

}
