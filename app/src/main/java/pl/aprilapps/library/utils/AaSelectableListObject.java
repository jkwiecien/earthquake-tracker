package pl.aprilapps.library.utils;

public class AaSelectableListObject {

	private boolean selected;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
