package pl.aprilapps.library.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by jacek on 25.05.2014.
 */
public class AaDisplayUtils extends AaUtils {

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static int getScreenHeightInPixels(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		if (isAtLeastApiLevel(13)) {
			Display display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			return size.y;
		} else {
			Display display = wm.getDefaultDisplay();
			return display.getHeight(); // deprecated
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static int getScreenWidthInPixels(Context context) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		if (isAtLeastApiLevel(13)) {
			Display display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			return size.x;
		} else {
			Display display = wm.getDefaultDisplay();
			return display.getWidth(); // deprecated
		}
	}

	public static int getScreenWidthInDp(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return pixelsToDp(context, displayMetrics.widthPixels);
	}

	public static int getScreenHeightInDp(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return pixelsToDp(context, displayMetrics.heightPixels);
	}

	public static int pixelsToDp(Context context, int px) {
		int dp = (int) (px / context.getResources().getDisplayMetrics().density);
		return dp;
	}

	public static int dpToPixels(Context context, int dp) {
		int px = (int) (dp * context.getResources().getDisplayMetrics().density);
		return px;
	}
}
