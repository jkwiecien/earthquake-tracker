package pl.aprilapps.library.ui.async;

import pl.aprilapps.library.ui.dialogs.AaDeterminedProgressDialog;

public abstract class AaDeterminedAsyncTask<Params, Progress, Result> extends AaAsyncTask<Params, Progress, Result> {

	protected AaDeterminedProgressDialog progressDialog;

	public AaDeterminedAsyncTask(IAaTask listener) {
		super(listener);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	protected Result doInBackground(Params... params) {
		return null;
	}

	;

	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		setFinished(true);
		try {
			if (progressDialog != null)
				progressDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AaDeterminedProgressDialog getProgressDialog() {
		return progressDialog;
	}

	public void setProgressDialog(AaDeterminedProgressDialog progressDialog) {
		this.progressDialog = progressDialog;
	}

	;

	@Override
	public void forceCancel() {
		super.forceCancel();
		try {
			if (progressDialog != null)
				progressDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
