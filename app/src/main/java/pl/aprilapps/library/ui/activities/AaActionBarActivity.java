package pl.aprilapps.library.ui.activities;

import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.async.AaAsyncTask;
import pl.aprilapps.library.ui.async.AaAsyncTask.IAaTask;
import pl.aprilapps.library.ui.async.AaDeterminedAsyncTask;
import pl.aprilapps.library.ui.async.AaUndeterminedAsyncTask;
import pl.aprilapps.library.ui.dialogs.AaDeterminedProgressDialog;
import pl.aprilapps.library.ui.dialogs.AaDialogFragment;
import pl.aprilapps.library.ui.dialogs.AaUndeterminedProgressDialog;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class AaActionBarActivity extends Activity implements IAaTask {

	public static final int ANIM_NONE = -1;

	protected AaAsyncTask<?, ?, ?> downloader;
	private AaDialogFragment progressDialog;
	private boolean visible;

	View progressLayout;

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getApp().registerActivity(this);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		super.onCreate(savedInstanceState);

		if (getApp().isRemovedFromMemory())
			return;

		downloader = (AaAsyncTask<?, ?, ?>) getLastNonConfigurationInstance();
		if (downloader != null)
			downloader.setListener(this);

		if (savedInstanceState != null) {
			AaUndeterminedProgressDialog undeterminedProgress = (AaUndeterminedProgressDialog) getFragmentManager().findFragmentByTag(AaUndeterminedProgressDialog.TAG);
			AaDeterminedProgressDialog determinedProgress = (AaDeterminedProgressDialog) getFragmentManager().findFragmentByTag(AaDeterminedProgressDialog.TAG);

			if (undeterminedProgress != null && downloader != null && downloader instanceof AaUndeterminedAsyncTask) {
				AaUndeterminedAsyncTask undeterminedTask = (AaUndeterminedAsyncTask) downloader;
				progressDialog = undeterminedProgress;
				undeterminedTask.setProgressDialog(undeterminedProgress);
				if (undeterminedTask.isFinished())
					undeterminedProgress.dismiss();
			} else if (determinedProgress != null && downloader != null && downloader instanceof AaDeterminedAsyncTask) {
				AaDeterminedAsyncTask determinedTask = (AaDeterminedAsyncTask) downloader;
				progressDialog = undeterminedProgress;
				determinedTask.setProgressDialog(determinedProgress);
				if (determinedTask.isFinished())
					determinedProgress.dismiss();
			}
		}
	}

	public AaAsyncTask<?, ?, ?> getDownloader() {
		return downloader;
	}

	public void setDownloader(AaAsyncTask<?, ?, ?> downloader) {
		// Log.v(AaApplication.TAG_LOG, "setDownloader");
		if (this.downloader != null)
			this.downloader.cancel(true);
		this.downloader = downloader;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onDownloaderStarted() {
		if (downloader == null)
			return;

		if (downloader instanceof AaDeterminedAsyncTask) {
			AaDeterminedAsyncTask determinedTask = (AaDeterminedAsyncTask) downloader;
			this.progressDialog = determinedTask.getProgressDialog();
		} else if (downloader instanceof AaUndeterminedAsyncTask) {
			AaUndeterminedAsyncTask indeterminedTask = (AaUndeterminedAsyncTask) downloader;
			this.progressDialog = indeterminedTask.getProgressDialog();
		}
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if (downloader != null)
			downloader.detach();
		return downloader;
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (getApp().isRemovedFromMemory())
			return;
		setProgressBarIndeterminateVisibility(false);
		visible = true;

		try {
			if (progressDialog != null && downloader == null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		visible = false;
	}

	/**
	 * Injects fragment into activity. For animation always both enter and exit animations must be
	 * provided.
	 * 
	 * @param fragment
	 *            Fragment to inject
	 * @param viewHolderId
	 *            Res id of container layout
	 * @param addToBackStack
	 *            Wheter fragment should be add to back stack
	 * @param replace
	 *            Wheter fragment should be replaced, even if already exist
	 */
	public void injectFragment(Fragment fragment, int viewHolderId, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation, boolean addToBackStack,
			boolean replace) {
		FragmentManager manager = getFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		if (enterAnimation != ANIM_NONE && exitAnimation != ANIM_NONE) {
			transaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
		}

		if (fragment.isAdded() && !replace) {
			transaction.attach(fragment);
		} else if (replace) {
			transaction.replace(viewHolderId, fragment, fragment.getClass().getSimpleName());
			if (addToBackStack) {
				transaction.addToBackStack(fragment.getClass().getSimpleName());
			}
		} else {
			transaction.add(viewHolderId, fragment, fragment.getClass().getSimpleName());
			if (addToBackStack) {
				transaction.addToBackStack(fragment.getClass().getSimpleName());
			}
		}
		transaction.commit();
	}

	/**
	 * Injects fragment into activity. For animation always both enter and exit animations must be
	 * provided.
	 * 
	 * @param fragment
	 *            Fragment to inject
	 * @param tag
	 *            Desired tag
	 * @param viewHolderId
	 *            Res id of container layout
	 * @param addToBackStack
	 *            Wheter fragment should be add to back stack
	 * @param replace
	 *            Wheter fragment should be replaced, even if already exist
	 */
	public void injectFragment(Fragment fragment, String tag, int viewHolderId, int enterAnimation, int exitAnimation, int popEnterAnimation, int popExitAnimation,
			boolean addToBackStack, boolean replace) {
		FragmentManager manager = getFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		if (enterAnimation != ANIM_NONE && exitAnimation != ANIM_NONE) {
			transaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
		}

		if (fragment.isAdded() && !replace) {
			transaction.attach(fragment);
		} else if (replace) {
			transaction.replace(viewHolderId, fragment, tag);
			if (addToBackStack) {
				transaction.addToBackStack(tag);
			}
		} else {
			transaction.add(viewHolderId, fragment, tag);
			if (addToBackStack) {
				transaction.addToBackStack(tag);
			}
		}
		transaction.commit();
	}

	@Override
	public void onDownloaderFinished() {
		downloader = null;
	}

	public AaApplication getApp() {
		return (AaApplication) getApplication();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			int backStackCount = getFragmentManager().getBackStackEntryCount();
			if (backStackCount <= 1)
				onHomePressed();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onHomePressed() {
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
		visible = false;
	}

	public boolean isVisible() {
		return visible;
	}

	@Override
	protected void onDestroy() {
		getApp().unregisterActivity(this);
		if (isDownloading())
			getDownloader().cancel(true);
		super.onDestroy();
	}

	public boolean isDownloading() {
		// Log.v(AaApplication.TAG_LOG, "isDownloading");
		return (getDownloader() != null);
	}

	public AaDialogFragment getProgressDialog() {
		return progressDialog;
	}

	public void setProgressDialog(AaDialogFragment progressDialog) {
		this.progressDialog = progressDialog;
	}

	@SuppressLint("NewApi")
	public boolean isTablet() {
		if (Build.VERSION.SDK_INT < 11)
			return false;

		Configuration config = getResources().getConfiguration();
		try {
			return (config.smallestScreenWidthDp >= 600);
		} catch (Exception e) {
			return false;
		}
	}

	public SharedPreferences getPreferences() {
		return getApp().getPreferences();
	}

	public Editor getPreferencesEditor() {
		return getApp().getPreferencesEditor();
	}

	public int getScreenWidthInDp() {
		return getApp().getScreenWidthInDp();
	}

	public int getScreenHeightInDp() {
		return getApp().getScreenHeightInDp();
	}

	public void showDialogFragment(AaDialogFragment dialog, Bundle args) {
		if (args != null)
			dialog.setArguments(args);
		dialog.show(getFragmentManager(), dialog.getClass().getSimpleName());
	}

	public void showToast(String text, int duration) {
		getApp().showToast(text, duration);
	}

	public void showProgressLayout(String message, View... viewsToHide) {
		if (progressLayout != null) {
			progressLayout.setVisibility(View.VISIBLE);

			TextView label = (TextView) progressLayout.findViewById(R.id.progress_label);
			if (label != null && message != null) {
				label.setText(message);
			}

			for (View view : viewsToHide) {
				view.setVisibility(View.INVISIBLE);
			}
		}
	}

	public void showProgressLayout(View... viewsToHide) {
		showProgressLayout(null, viewsToHide);
	}

	public void hideProgressLayout(View... viewsToShow) {
		if (progressLayout != null) {
			progressLayout.setVisibility(View.INVISIBLE);

			for (View view : viewsToShow) {
				view.setVisibility(View.VISIBLE);
			}
		}
	}

	public void initProgressLayout() {
		progressLayout = findViewById(R.id.progress_layout);
        if (progressLayout != null) {
            progressLayout.setVisibility(View.INVISIBLE);
        }
	}

    private View.OnClickListener progressLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onProgressLayoutTapped();
        }
    };

    public void registerProgressLayoutClickListener() {
        if (progressLayout != null) {
            progressLayout.setOnClickListener(progressLayoutClickListener);
        }
    }

    public void unregisterProgressLayoutClickListener() {
        if (progressLayout != null) {
            progressLayout.setOnClickListener(null);
        }
    }

	public void onProgressLayoutTapped() {

	}

    public View getProgressLayout() {
        return progressLayout;
    }
}
