package pl.aprilapps.library.ui;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings.System;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.library.utils.AaDisplayUtils;

public abstract class AaApplication extends Application {

	public static final String TAG_LOG = "LOG_AA";

	private ArrayList<Activity> activityPool;

	private Handler toastHandler;
	private Toast toast;

	public static void closeInput(final View caller) {
		caller.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) caller.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(caller.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 100);
	}

	public static void showInput(final View caller) {
		caller.postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) caller.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.showSoftInput(caller, InputMethodManager.SHOW_IMPLICIT);
			}
		}, 100);
	}

	public static String getAppHashKey(Context context) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
			MessageDigest md = MessageDigest.getInstance("SHA");
			for (Signature signature : info.signatures) {
				md.update(signature.toByteArray());
			}
			return Base64.encodeToString(md.digest(), Base64.DEFAULT);
		} catch (NameNotFoundException e) {
			return null;
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static boolean isGingerbread() {
		return (Build.VERSION.SDK_INT < 11);
	}

	public static boolean isTelephonyEnabled(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm != null && tm.getSimState() == TelephonyManager.SIM_STATE_READY;
	}

	@SuppressWarnings("deprecation")
	public static String getDeviceUUID(Context context) {
		return System.getString(context.getContentResolver(), System.ANDROID_ID);
	}

	public static LayoutInflater getLayoutInflater(Context context) {
		return (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * Wymaga <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
	 * 
	 * @param context
	 * @return true jezli jest dostep do internetu
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static boolean isServerAvailible(String url) throws ClientProtocolException, IOException {
		HttpGet httpGet = new HttpGet(url);
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used.
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpResponse response = httpClient.execute(httpGet);

		return (response.getStatusLine().getStatusCode() == 200);
	}

	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	public static boolean isExternalStorageAvailible() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state))
			return true;
		else
			return false;
	}

	public static SharedPreferences getStaticPreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	/**
	 * Rejestruje activity w liscie zainicjowanych
	 * 
	 * @param activity
	 */
	public void registerActivity(Activity activity) {
		if (activityPool == null)
			activityPool = new ArrayList<Activity>();

		if (!activityPool.contains(activity))
			activityPool.add(activity);
	}

	/**
	 * Wyrejestrowuje activity z listy inicjowanych
	 * 
	 * @param activity
	 */
	public void unregisterActivity(Activity activity) {
		if (activityPool == null)
			return;

		activityPool.remove(activity);
	}

	public void killApp() {
		for (Activity activity : activityPool) {
			activity.finish();
		}
	}

	public int getPoolCount() {
		return activityPool.size();
	}

    public ArrayList<Activity> getActivityPool() {
        return activityPool;
    }

    public boolean isRemovedFromMemory() {
		return (activityPool == null);
	}

	public String getVersionName() {
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			return pInfo.versionName;
		} catch (NameNotFoundException e) {
			return null;
		}
	}

	public int getVersion() {
		int v = 1;
		try {
			v = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			// Huh? Really?
		}
		return v;
	}

	public boolean isTestRelease() {
		return (getVersionName().contains("T"));
	}

	public void showSoftInput(Activity activity, EditText callerInput) {
		if (!hasDevideHardwareKeyboard()) {
			callerInput.requestFocus();
			activity.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		}
	}

	public boolean hasDevideHardwareKeyboard() {
		Configuration config = getResources().getConfiguration();
		return (config.keyboard != Configuration.KEYBOARD_NOKEYS);
	}

	public int getScreenHeightInPixels() {
		return AaDisplayUtils.getScreenHeightInPixels(getApplicationContext());
	}

	public int getScreenWidthInPixels() {
		return AaDisplayUtils.getScreenWidthInPixels(getApplicationContext());
	}

	public int getScreenWidthInDp() {
		return AaDisplayUtils.getScreenWidthInDp(getApplicationContext());
	}

	public int getScreenHeightInDp() {
		return AaDisplayUtils.getScreenHeightInDp(getApplicationContext());
	}

	public int pixelsToDp(int px) {
		return AaDisplayUtils.pixelsToDp(getApplicationContext(), px);
	}

	public int dpToPixels(int dp) {
		return AaDisplayUtils.dpToPixels(getApplicationContext(), dp);
	}

	public SharedPreferences getPreferences() {
		return PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	}

	public Editor getPreferencesEditor() {
		return getPreferences().edit();
	}

	/**
	 * Requires GET_TASKS permission
	 * 
	 * @return true if the app is in foreground, false otherwise
	 */
	public boolean isAppInForeground() {
		ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		// The first in the list of RunningTasks is always the foreground task.
		RunningTaskInfo foregroundTaskInfo = am.getRunningTasks(1).get(0);

		if (foregroundTaskInfo == null) {
			return false;
		} else {
			try {
				String foregroundTaskPackageName = foregroundTaskInfo.topActivity.getPackageName();
				PackageManager pm = getPackageManager();
				PackageInfo foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0);
				String foregroundTaskAppName = foregroundAppPackageInfo.applicationInfo.loadLabel(pm).toString();
				return foregroundTaskAppName.equals(getAppName());
			} catch (Exception e) {
				return false;
			}
		}
	}

	public abstract String getAppName();

	public void showToast(final String text, final int duration) {
		if (isAppInForeground()) {
			if (toastHandler == null) {
				toastHandler = new Handler(Looper.getMainLooper());
			}
			toastHandler.post(new Runnable() {

				@Override
				public void run() {
					if (AaApplication.this.toast != null) {
						AaApplication.this.toast.cancel();
					}
					AaApplication.this.toast = Toast.makeText(getApplicationContext(), text, duration);
					toast.show();
				}
			});
		}
	}

}
