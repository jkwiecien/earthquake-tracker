package pl.aprilapps.library.ui.fragments;

import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.activities.AaActionBarActivity;
import pl.aprilapps.library.ui.async.AaAsyncTask;
import pl.aprilapps.library.ui.async.AaAsyncTask.IAaTask;
import pl.aprilapps.library.ui.async.AaDeterminedAsyncTask;
import pl.aprilapps.library.ui.async.AaUndeterminedAsyncTask;
import pl.aprilapps.library.ui.dialogs.AaDialogFragment;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class AaFragment extends Fragment implements IAaTask {

	protected AaAsyncTask<?, ?, ?> downloader;
	private View progressLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}

	public AaActionBarActivity getAaActivity() {
		return (AaActionBarActivity) getActivity();
	}

	public ActionBar getSupportActionBar() {
		return getAaActivity().getActionBar();
	}

	public boolean isDownloading() {
		return (downloader != null);
	}

	public AaApplication getApp() {
		return (AaApplication) getActivity().getApplication();
	}

	public AaAsyncTask<?, ?, ?> getDownloader() {
		return downloader;
	}

	public void setDownloader(AaAsyncTask<?, ?, ?> downloader) {
		if (this.downloader != null)
			this.downloader.cancel(true);
		this.downloader = downloader;
	}

	public AaDialogFragment getProgressDialog() {
		return getAaActivity().getProgressDialog();
	}

	public void setProgressDialog(AaDialogFragment dialog) {
		getAaActivity().setProgressDialog(dialog);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onDownloaderStarted() {
		if (getDownloader() == null)
			return;

		if (getDownloader() instanceof AaDeterminedAsyncTask) {
			AaDeterminedAsyncTask determinedTask = (AaDeterminedAsyncTask) getDownloader();
			setProgressDialog(determinedTask.getProgressDialog());
		} else if (getDownloader() instanceof AaUndeterminedAsyncTask) {
			AaUndeterminedAsyncTask indeterminedTask = (AaUndeterminedAsyncTask) getDownloader();
			setProgressDialog(indeterminedTask.getProgressDialog());
		}
	}

	@Override
	public void onDownloaderFinished() {
		setDownloader(null);
	}

	@Override
	public void onDetach() {
		if (downloader != null && downloader.equals(getDownloader()))
			downloader.forceCancel();
		super.onDetach();
	}

	public boolean isTablet() {
		return getAaActivity().isTablet();
	}

	public LayoutInflater getLayoutInflater() {
		return getAaActivity().getLayoutInflater();
	}

	public SharedPreferences getPreferences() {
		return getApp().getPreferences();
	}

	public Editor getPreferencesEditor() {
		return getApp().getPreferencesEditor();
	}

	public int getScreenWidthInDp() {
		return getApp().getScreenWidthInDp();
	}

	public int getScreenHeightInDp() {
		return getApp().getScreenHeightInDp();
	}

	public int pixelsToDp(int pixels) {
		return getApp().pixelsToDp(pixels);
	}

	public int doToPixels(int dp) {
		return getApp().dpToPixels(dp);
	}

	public void showDialogFragment(AaDialogFragment dialog, Bundle args) {
		getAaActivity().showDialogFragment(dialog, args);
	}

	public void showToast(String text, int duration) {
		getApp().showToast(text, duration);
	}

	public void showProgressLayout(String message, View... viewsToHide) {
		if (progressLayout != null) {
			progressLayout.setVisibility(View.VISIBLE);

			TextView label = (TextView) progressLayout.findViewById(R.id.progress_label);
			if (label != null && message != null) {
				label.setText(message);
			}

			for (View view : viewsToHide) {
				view.setVisibility(View.INVISIBLE);
			}
		}
	}

	public void showProgressLayout(View... viewsToHide) {
		showProgressLayout(null, viewsToHide);
	}

	public void hideProgressLayout(View... viewsToShow) {
		if (progressLayout != null) {
			progressLayout.setVisibility(View.INVISIBLE);

			for (View view : viewsToShow) {
				view.setVisibility(View.VISIBLE);
			}
		}
	}

	public void initProgressLayout(View fragmentView) {
		progressLayout = fragmentView.findViewById(R.id.progress_layout);
        if (progressLayout != null) {
            progressLayout.setVisibility(View.INVISIBLE);
        }
	}

    private View.OnClickListener progressLayoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onProgressLayoutTapped();
        }
    };

    public void registerProgressLayoutClickListener() {
        if (progressLayout != null) {
            progressLayout.setOnClickListener(progressLayoutClickListener);
        }
    }

    public void unregisterProgressLayoutClickListener() {
        if (progressLayout != null) {
            progressLayout.setOnClickListener(null);
        }
    }

    public void onProgressLayoutTapped() {

    }

    public View getProgressLayout() {
        return progressLayout;
    }
}
