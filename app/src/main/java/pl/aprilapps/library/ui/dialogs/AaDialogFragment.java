package pl.aprilapps.library.ui.dialogs;

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.activities.AaActionBarActivity;

public class AaDialogFragment extends DialogFragment {

	public AaActionBarActivity getAaActivity() {
		return (AaActionBarActivity) getActivity();
	}

	public AaApplication getApp() {
		return getAaActivity().getApp();
	}

	public SharedPreferences getPreferences() {
		return getApp().getPreferences();
	}

	public Editor getPreferencesEditor() {
		return getApp().getPreferencesEditor();
	}

	public int getScreenWidthInDp() {
		return getApp().getScreenWidthInDp();
	}

	public int getScreenHeightInDp() {
		return getApp().getScreenHeightInDp();
	}

	public void showToast(String text, int duration) {
		getApp().showToast(text, duration);
	}

}
