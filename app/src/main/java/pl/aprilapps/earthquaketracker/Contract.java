package pl.aprilapps.earthquaketracker;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by jacek on 23.08.2014.
 */


public interface Contract {

    public interface Tables {
        public final String EARTHQUAKES = "earthquakes";
        public final String TWEETS = "tweets";
    }

    public interface Common {
        public final String CREATED_AT = "created_at";
        public final String ACTIVE = "active";
        public final String UPDATED = "updated";
        public final String INTERNAL_ID = "_id";
    }

    public interface Earthquake {
        public final String ID = "id";
        public final String PLACE = "place";
        public final String MAGNITUDE = "magnitude";
        public final String TIME = "time";
        public final String URL = "url";
        public final String STATUS = "status";
        public final String TSUNAMI = "tsunami";
        public final String MAGNITUDE_TYPE = "magnitude_type";
        public final String LONGITUDE = "longitude";
        public final String LATITUDE = "latitude";
        public final String DEPTH = "depth";
    }

    public interface Tweet {
        public final String TEXT = "text";
        public final String SOURCE = "source";
        public final String USER = "user";
        public final String PLACE = "place";
        public final String FAVOURITE_COUNT = "favourite_count";
    }


}
