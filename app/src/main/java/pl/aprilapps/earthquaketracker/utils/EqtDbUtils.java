package pl.aprilapps.earthquaketracker.utils;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import pl.aprilapps.earthquaketracker.model.AbstractBean;
import pl.aprilapps.earthquaketracker.model.EqtDatabaseHelper;

/**
 * Created by jacek on 23.08.2014.
 */
public class EqtDbUtils {

    public static EqtDatabaseHelper getDbHelper(Context context) {
        return EqtDatabaseHelper.getInstance(context);
    }

    public static <T extends AbstractBean> Dao<T, String> getDao(Class<T> beanClass, Context context) throws SQLException {
        return getDbHelper(context).getDao(beanClass);
    }
}
