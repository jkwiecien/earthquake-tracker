package pl.aprilapps.earthquaketracker.tasks;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import pl.aprilapps.earthquaketracker.Constants;
import pl.aprilapps.earthquaketracker.Contract;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import pl.aprilapps.earthquaketracker.requests.EarthquakesRequest;
import pl.aprilapps.earthquaketracker.utils.EqtDbUtils;
import pl.aprilapps.library.ui.async.AaAsyncTask;
import pl.aprilapps.library.utils.GAEResponseObject;

/**
 * Created by jacek on 23.08.2014.
 */
public class LoadEarthquakesTask extends AaAsyncTask<Void, Void, GAEResponseObject<List<Earthquake>>> implements Constants{

    public interface ILoadEarthquakesTask extends IAaTask {
        public void onLoadEarthquakesResponse(GAEResponseObject<List<Earthquake>> response);
    }

    static final String LOG_TAG = LoadEarthquakesTask.class.getSimpleName();

    boolean tsunami;

    @Override
    protected GAEResponseObject<List<Earthquake>> doInBackground(Void... params) {
        Dao<Earthquake, String> earthquakeDao = null;
        QueryBuilder<Earthquake, String> earthquakeQb = null;
        try {
            earthquakeDao = EqtDbUtils.getDao(Earthquake.class, getActivity());
            earthquakeQb = earthquakeDao.queryBuilder();

            try {
                EarthquakesRequest request = new EarthquakesRequest(getActivity(), tsunami);
                List<Earthquake> list = request.getResponseObject();

                //checking if downloaded data is already cached in db, and caching if necessary
                for (Earthquake earthquake : list) {
                    earthquakeQb.where().idEq(earthquake.getId());
                    Earthquake bean = earthquakeDao.queryForFirst(earthquakeQb.prepare());
                    if (bean == null || earthquake.getUpdated().after(bean.getUpdated())) {
                        earthquakeDao.createOrUpdate(earthquake);
                    }
                }

                return new GAEResponseObject<List<Earthquake>>(list);
            } catch (Exception e) {
                //Connection problem occurred, using database to load data
                Log.w(LOG_TAG, "Error loading data from internet. Using local database. " + e.getMessage());

                if (tsunami) {
                    earthquakeQb.where().eq(Contract.Common.ACTIVE, true).and().eq(Contract.Earthquake.TSUNAMI, true);
                } else {
                    earthquakeQb.where().eq(Contract.Common.ACTIVE, true);
                }
                earthquakeQb.limit(DISPLAYED_EARTHQUAKES);
                earthquakeQb.orderBy(Contract.Earthquake.TIME, false).orderBy(Contract.Earthquake.MAGNITUDE, false);

                PreparedQuery<Earthquake> preparedQuery = earthquakeQb.prepare();
                return new GAEResponseObject<List<Earthquake>>(earthquakeDao.query(preparedQuery));
            }

        } catch (SQLException e) {
            Log.e(LOG_TAG, "Database error. Cannot proceed. " + e.getMessage());
            return new GAEResponseObject<List<Earthquake>>(500);
        }
    }

    @Override
    protected void onPostExecute(GAEResponseObject<List<Earthquake>> response) {
        super.onPostExecute(response);
        getListener().onLoadEarthquakesResponse(response);
    }

    public LoadEarthquakesTask(ILoadEarthquakesTask listener, boolean tsunami) {
        super(listener);
        this.tsunami = tsunami;
    }

    @Override
    public ILoadEarthquakesTask getListener() {
        return (ILoadEarthquakesTask) super.getListener();
    }
}
