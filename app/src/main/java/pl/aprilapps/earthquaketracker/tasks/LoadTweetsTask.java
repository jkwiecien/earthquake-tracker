package pl.aprilapps.earthquaketracker.tasks;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.earthquaketracker.Constants;
import pl.aprilapps.earthquaketracker.Contract;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import pl.aprilapps.earthquaketracker.model.Tweet;
import pl.aprilapps.earthquaketracker.utils.EqtDbUtils;
import pl.aprilapps.library.ui.async.AaAsyncTask;
import pl.aprilapps.library.utils.GAEResponseObject;
import twitter4j.GeoLocation;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

/**
 * Created by jacek on 23.08.2014.
 */
public class LoadTweetsTask extends AaAsyncTask<Void, Void, GAEResponseObject<List<Tweet>>> implements Constants {

	public interface ILoadTweetsTaskTask extends IAaTask {
		public void onLoadTweetsTaskResponse(GAEResponseObject<List<Tweet>> response);
	}

	static final String LOG_TAG = LoadTweetsTask.class.getSimpleName();

	Earthquake earthquake;

	@Override
	protected GAEResponseObject<List<Tweet>> doInBackground(Void... params) {
		Dao<Tweet, String> dao = null;
		QueryBuilder<Tweet, String> qb = null;

        List<Tweet> tweets = new ArrayList<Tweet>();
		try {
			dao = EqtDbUtils.getDao(Tweet.class, getActivity());
			qb = dao.queryBuilder();

			try {
				ConfigurationBuilder cb = new ConfigurationBuilder();
				cb.setDebugEnabled(true).setOAuthConsumerKey("9sP4ogIZm2hiNE1NCNvDME8Yg").setOAuthConsumerSecret("FVbe0YUSyJYX885PzA8qyr4FQpgljmIq9K768Z3YWnOD8eKOOm")
						.setOAuthAccessToken("2759223583-6HgoPpjObH1A4M5TMvRaIgg8UVKesR6Cm4l9Tlo").setOAuthAccessTokenSecret("fAbwmVvr80ivCo76sR14CpZD9EOMMGOVjAVkpcRcd0aSP");

				TwitterFactory tf = new TwitterFactory(cb.build());
				Twitter twitter = tf.getInstance();

				Query query = new Query();
				query.setGeoCode(new GeoLocation(earthquake.getLatitude(), earthquake.getLongitude()), 25, Query.Unit.km);
				query.setCount(DISPLAYED_TWEETS);
				QueryResult queryResult = twitter.search(query);

				List<twitter4j.Status> statuses = queryResult.getTweets();
                if (tweets != null) {
                    for (twitter4j.Status status : statuses) {
                        Tweet tweet = new Tweet(status);
                        tweets.add(tweet);
                        dao.createOrUpdate(tweet);
                    }
                }

				return new GAEResponseObject<List<Tweet>>(tweets);
			} catch (Exception e) {
//				// Connection problem occurred, using database to load data
//				Log.w(LOG_TAG, "Error loading data from internet. Using local database. " + e.getMessage());
//
//                qb.where().eq(Contract.Common.ACTIVE, true).and().eq(Contract.Earthquake.TSUNAMI, true);
//				qb.limit(DISPLAYED_EARTHQUAKES);
//				qb.orderBy(Contract.Earthquake.MAGNITUDE, true);
//
//				PreparedQuery<Earthquake> preparedQuery = qb.prepare();
//				return new GAEResponseObject<List<Tweet>>(dao.query(preparedQuery));

                return new GAEResponseObject<List<Tweet>>(500);
			}

		} catch (SQLException e) {
			Log.e(LOG_TAG, "Database error. Cannot proceed. " + e.getMessage());
			return new GAEResponseObject<List<Tweet>>(500);
		}
	}

	@Override
	protected void onPostExecute(GAEResponseObject<List<Tweet>> response) {
		super.onPostExecute(response);
		getListener().onLoadTweetsTaskResponse(response);
	}

	public LoadTweetsTask(ILoadTweetsTaskTask listener, Earthquake earthquake) {
		super(listener);
		this.earthquake = earthquake;
	}

	@Override
	public ILoadTweetsTaskTask getListener() {
		return (ILoadTweetsTaskTask) super.getListener();
	}
}
