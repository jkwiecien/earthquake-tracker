package pl.aprilapps.earthquaketracker.fragments;

import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.activities.EarthquakeDetailsActivity;
import pl.aprilapps.earthquaketracker.activities.EqtActivity;
import pl.aprilapps.earthquaketracker.activities.HomeActivity;
import pl.aprilapps.earthquaketracker.adapters.EarthquakesAdapter;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import pl.aprilapps.earthquaketracker.tasks.LoadEarthquakesTask;
import pl.aprilapps.library.ui.activities.AaActionBarActivity;
import pl.aprilapps.library.utils.GAEResponseObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by jacek on 23.08.2014.
 */
public class EarthquakesFragment extends EqtFragment implements LoadEarthquakesTask.ILoadEarthquakesTask {

	@InjectView(R.id.list_view)
	ListView listView;

	EarthquakesAdapter adapter;
	List<Earthquake> earthquakes;

    boolean loaded = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		earthquakes = new ArrayList<Earthquake>();
		adapter = new EarthquakesAdapter(getActivity(), earthquakes);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_earthquakes, container, false);
		ButterKnife.inject(this, view);
		initProgressLayout(view);


        listView.setAdapter(adapter);
        listView.setOnItemClickListener(listViewItemClickListener);

		return view;
	}

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!loaded) {
            loadEarthquakes(false);
        }
    }

    @Override
	public void onLoadEarthquakesResponse(GAEResponseObject<List<Earthquake>> response) {
		if (response.isOk()) {
            earthquakes.clear();
            loaded = true;
            List<Earthquake> objects = response.getObject();
            if (objects.size() > 0) {
                hideProgressLayout(listView);
                earthquakes.addAll(response.getObject());
                adapter.notifyDataSetChanged();
            } else {
                registerProgressLayoutClickListener();
                showProgressLayout(getString(R.string.error_no_data_to_display), listView);
                hideProgressLayoutProgressBar();
            }
		} else {
			registerProgressLayoutClickListener();
			showProgressLayout(getString(R.string.loading_unknown_error), listView);
            hideProgressLayoutProgressBar();
		}
	}

	public void loadEarthquakes(boolean tsunami) {
		unregisterProgressLayoutClickListener();
		showProgressLayout(getString(R.string.loading_message), listView);

        if (isDownloading()) {
            getDownloader().forceCancel();
        }
		LoadEarthquakesTask task = new LoadEarthquakesTask(this, tsunami);
		setDownloader(task);
		task.execute();
	}

    @Override
    public void onProgressLayoutTapped() {
        super.onProgressLayoutTapped();
        loadEarthquakes(getAaActivity().isTsunamiChecked());
    }

    @Override
    public HomeActivity getAaActivity() {
        return (HomeActivity) super.getAaActivity();
    }

    AdapterView.OnItemClickListener listViewItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Earthquake earthquake = (Earthquake) parent.getAdapter().getItem(position);
            Intent intent = new Intent(getActivity(), EarthquakeDetailsActivity.class);
            intent.putExtra(KEY_EARTHQUAKE, earthquake);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
        }
    };
}
