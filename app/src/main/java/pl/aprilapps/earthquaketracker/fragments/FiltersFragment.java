package pl.aprilapps.earthquaketracker.fragments;

import butterknife.InjectView;
import butterknife.OnClick;
import pl.aprilapps.earthquaketracker.R;

import android.app.ActionBar;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.ButterKnife;
import pl.aprilapps.earthquaketracker.activities.HomeActivity;
import pl.aprilapps.library.ui.activities.AaActionBarActivity;

/**
 * Created by jacek on 23.08.2014.
 */
public class FiltersFragment extends EqtFragment {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle mDrawerToggle;

    @InjectView(R.id.tsunami_checkbox)
    CheckBox tsunamiCheckbox;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_filters, container, false);
		ButterKnife.inject(this, view);

		return view;
	}

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        View containerView = getActivity().findViewById(fragmentId);
        this.drawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        this.drawerLayout.setDrawerShadow(R.drawable.right_drawer_shadow, GravityCompat.END);
        // set up the drawer's list view with items and click listener


        tsunamiCheckbox.setOnCheckedChangeListener(tsunamiCheckboxListener);

        this.drawerLayout.setDrawerListener(mDrawerToggle);
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    @OnClick(R.id.content)
    void onContentClicked() {
        //dummy method preventing clicking what's below the drawer
    }

    CompoundButton.OnCheckedChangeListener tsunamiCheckboxListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            getAaActivity().reloadEarthquakes();
        }
    };

    @Override
    public HomeActivity getAaActivity() {
        return (HomeActivity) super.getAaActivity();
    }

    public boolean isTsunamiChecked() {
        return tsunamiCheckbox.isChecked();
    }
}
