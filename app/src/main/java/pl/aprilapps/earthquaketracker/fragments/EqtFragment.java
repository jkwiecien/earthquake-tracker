package pl.aprilapps.earthquaketracker.fragments;

import android.os.Bundle;
import android.view.View;

import pl.aprilapps.earthquaketracker.Constants;
import pl.aprilapps.earthquaketracker.EqT;
import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.library.ui.AaApplication;
import pl.aprilapps.library.ui.fragments.AaFragment;

/**
 * Created by jacek on 23.08.2014.
 */
public class EqtFragment extends AaFragment implements Constants{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public EqT getApp() {
        return (EqT) super.getApp();
    }

    public void hideProgressLayoutProgressBar() {
        if (getProgressLayout() != null) {
            getActivity().findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
        }
    }

    public void showProgressLayoutProgressBar() {
        if (getProgressLayout() != null) {
            getActivity().findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        }
    }
}
