package pl.aprilapps.earthquaketracker.fragments;

import java.text.SimpleDateFormat;

import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.activities.EarthquakeDetailsActivity;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by jacek on 23.08.2014.
 */
public class EarthquakeDetailsInfoFragment extends EqtFragment {

	@InjectView(R.id.place_label)
	TextView placeLabel;
	@InjectView(R.id.time_label)
	TextView timeLabel;
	@InjectView(R.id.magnitude_label)
	TextView magnitudeLabel;
	@InjectView(R.id.magnitude_type_label)
	TextView magnitudeTypeLabel;
	@InjectView(R.id.status_label)
	TextView statusLabel;
	@InjectView(R.id.tsunami_label)
	TextView tsunamiLabel;
	@InjectView(R.id.longitude_label)
	TextView longitudeLabel;
	@InjectView(R.id.latitude_label)
	TextView latitudeLabel;
	@InjectView(R.id.depth_label)
	TextView depthLabel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_details_info, container, false);
		ButterKnife.inject(this, view);

		SimpleDateFormat df = new SimpleDateFormat(TIME_PATTERN);

		placeLabel.setText(getEarthquake().getPlace());
		timeLabel.setText(df.format(getEarthquake().getTime()) + " UTC");
		magnitudeLabel.setText(Double.toString(getEarthquake().getMagnitude()));
		magnitudeTypeLabel.setText(getEarthquake().getMagnitudeType());
		statusLabel.setText(getEarthquake().getStatus());
		tsunamiLabel.setText(getEarthquake().isTsunami() ? getString(R.string.YES) : getString(R.string.NO));
		longitudeLabel.setText(Double.toString(getEarthquake().getLongitude()));
		latitudeLabel.setText(Double.toString(getEarthquake().getLatitude()));
		depthLabel.setText(Double.toString(getEarthquake().getDepth()));

		return view;
	}

	@Override
	public EarthquakeDetailsActivity getAaActivity() {
		return (EarthquakeDetailsActivity) super.getAaActivity();
	}

	private Earthquake getEarthquake() {
		return getAaActivity().getEarthquake();
	}
}
