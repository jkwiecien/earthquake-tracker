package pl.aprilapps.earthquaketracker.fragments;

import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.activities.EarthquakeDetailsActivity;

import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pl.aprilapps.earthquaketracker.adapters.TweetsAdapter;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import pl.aprilapps.earthquaketracker.model.Tweet;
import pl.aprilapps.earthquaketracker.tasks.LoadTweetsTask;
import pl.aprilapps.library.utils.GAEResponseObject;

/**
 * Created by jacek on 23.08.2014.
 */
public class TweetsFragment extends EqtFragment implements LoadTweetsTask.ILoadTweetsTaskTask {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle mDrawerToggle;

    @InjectView(R.id.list_view)
    ListView listView;

    List<Tweet> tweets;
    TweetsAdapter adapter;

    boolean loaded = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tweets = new ArrayList<Tweet>();
        adapter = new TweetsAdapter(getActivity(), tweets);
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_tweets, container, false);
		ButterKnife.inject(this, view);
        initProgressLayout(view);

        listView.setAdapter(adapter);

		return view;
	}

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!loaded) {
            loadTweets();
        }
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        View containerView = getActivity().findViewById(fragmentId);
        this.drawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        this.drawerLayout.setDrawerShadow(R.drawable.right_drawer_shadow, GravityCompat.END);
        // set up the drawer's list view with items and click listener

        this.drawerLayout.setDrawerListener(mDrawerToggle);
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    @OnClick(R.id.content)
    void onContentClicked() {
        //dummy method preventing clicking what's below the drawer
    }

    @Override
    public EarthquakeDetailsActivity getAaActivity() {
        return (EarthquakeDetailsActivity) super.getAaActivity();
    }

    public void loadTweets() {
        unregisterProgressLayoutClickListener();
        showProgressLayout(getString(R.string.loading_message), listView);

        if (isDownloading()) {
            getDownloader().forceCancel();
        }
        LoadTweetsTask task = new LoadTweetsTask(this, getEarthquake());
        setDownloader(task);
        task.execute();
    }

    private Earthquake getEarthquake() {
        return getAaActivity().getEarthquake();
    }

    @Override
    public void onLoadTweetsTaskResponse(GAEResponseObject<List<Tweet>> response) {
        if (response.isOk()) {
            tweets.clear();
            loaded = true;
            List<Tweet> objects = response.getObject();
            if (objects.size() > 0) {
                hideProgressLayout(listView);
                tweets.addAll(response.getObject());
                adapter.notifyDataSetChanged();
            } else {
                registerProgressLayoutClickListener();
                showProgressLayout(getString(R.string.error_no_data_to_display), listView);
                hideProgressLayoutProgressBar();
            }
        } else {
            registerProgressLayoutClickListener();
            showProgressLayout(getString(R.string.error_loading_tweets), listView);
            hideProgressLayoutProgressBar();
        }
    }

    @Override
    public void onProgressLayoutTapped() {
        super.onProgressLayoutTapped();
        loadTweets();
    }
}
