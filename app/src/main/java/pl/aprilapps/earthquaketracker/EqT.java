package pl.aprilapps.earthquaketracker;

import pl.aprilapps.earthquaketracker.model.EqtDatabaseHelper;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.util.Log;

/**
 * Created by jacek on 23.08.2014.
 */
public class EqT extends pl.aprilapps.library.ui.AaApplication {

    static final String LOG_TAG = EqT.class.getSimpleName();

	// The account name
	public static final String ACCOUNT = "Earthquake Tracker";
	// Instance fields
	Account syncAccount;

	@Override
    public void onCreate() {
        super.onCreate();
        EqtDatabaseHelper dbHelper = EqtDatabaseHelper.getInstance(this);
        dbHelper.getWritableDatabase();
    }

	@Override
	public String getAppName() {
		return getString(R.string.app_name);
	}


    public void createSyncAccount() {
        // Create the account type and default account
        Account newAccount = new Account(ACCOUNT, getString(R.string.sync_account_type));

        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            Log.i(LOG_TAG, "SyncAccount created.");
        } else {
            Log.w(LOG_TAG, "SyncAccount creation failed. Account might has been already created.");
        }

        syncAccount = newAccount;
    }

    public Account getSyncAccount() {
        return syncAccount;
    }
}
