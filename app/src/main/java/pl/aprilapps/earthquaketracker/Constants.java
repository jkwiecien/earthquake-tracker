package pl.aprilapps.earthquaketracker;

/**
 * Created by jacek on 23.08.2014.
 */
public interface Constants {

	public final String TIME_PATTERN = "yyyy-MM-dd HH:mm";

	public final String KEY_EARTHQUAKE = "earthquake";

	public final long DISPLAYED_EARTHQUAKES = 20;
	public final int DISPLAYED_TWEETS = 20;

	public final int REQ_GMS_UNAVAILABLE_DIALOG = 924;

	public final long SYNC_INTERVAL_IN_MINUTES = 20L;
	public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * 60L;
}
