package pl.aprilapps.earthquaketracker.requests;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import pl.aprilapps.earthquaketracker.model.Earthquake;
import pl.aprilapps.library.utils.AaJsonUtils;
import pl.aprilapps.library.utils.communication.AaGetRequest;

/**
 * Created by jacek on 23.08.2014.
 */
public class EarthquakesRequest extends AaGetRequest<List<Earthquake>> {
    public EarthquakesRequest(Context context, boolean tsunami) throws MalformedURLException {
        super("week", context);
        this.tsunami = tsunami;
    }

    boolean tsunami;

    @Override
    public List<Earthquake> parseResponse(String responseString) throws Exception {
        List<Earthquake> list = new ArrayList<Earthquake>();

        //removing non-json String fragments.
        responseString = responseString.replace("eqfeed_callback(", "").replace(");", "");

        JSONObject json = new JSONObject(responseString);
        JSONArray features = json.optJSONArray("features");
        features = AaJsonUtils.sort(features, new Comparator() {
            @Override
            public int compare(Object a, Object b) {
                JSONObject jsonA = (JSONObject) a;
                JSONObject jsonB = (JSONObject) b;

                Double magA = jsonA.optJSONObject("properties").optDouble("mag");
                Double magB = jsonB.optJSONObject("properties").optDouble("mag");

                return magB.compareTo(magA);
            }
        });

        int featuresCount = features.length();
        int i = 0;
        while (list.size() < 21 && i < featuresCount) {
            Earthquake earthquake = new Earthquake(features.optJSONObject(i));
            if (tsunami && earthquake.isTsunami()) {
                list.add(earthquake);
            } else if (!tsunami) {
                list.add(earthquake);
            }
            i++;
        }

        return list;
    }

    @Override
    public String getAPIUrl() {
        return "http://earthquake.usgs.gov/earthquakes/feed/geojsonp/2.5";
    }
}
