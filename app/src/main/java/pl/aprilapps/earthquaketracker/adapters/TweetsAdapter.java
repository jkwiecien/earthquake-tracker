package pl.aprilapps.earthquaketracker.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import pl.aprilapps.earthquaketracker.Constants;
import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.aprilapps.earthquaketracker.model.Tweet;

public class TweetsAdapter extends ArrayAdapter<Tweet> implements Constants{

    static SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_PATTERN);

	public TweetsAdapter(Context context, List<Tweet> objects) {
		super(context, R.layout.cell_tweet, objects);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder vh;
        Tweet object = getItem(position);

		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			view = inflater.inflate(R.layout.cell_tweet, null, true);
			vh = new ViewHolder(view);
			view.setTag(vh);
		} else
			vh = (ViewHolder) view.getTag();
		if (object == null)
			return view;

		vh.userLabel.setText(object.getUser());
        vh.timeLabel.setText(dateFormat.format(object.getCreatedAt()));
        vh.textLabel.setText(object.getText());

		return view;
	}

	class ViewHolder {
		@InjectView(R.id.user_label)
		TextView userLabel;
		@InjectView(R.id.time_label)
		TextView timeLabel;
		@InjectView(R.id.text_label)
		TextView textLabel;

		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
		}
	}

}
