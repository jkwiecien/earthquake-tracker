package pl.aprilapps.earthquaketracker.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import pl.aprilapps.earthquaketracker.Constants;
import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.model.Earthquake;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class EarthquakesAdapter extends ArrayAdapter<Earthquake> implements Constants{

	static SimpleDateFormat dateFormat = new SimpleDateFormat(TIME_PATTERN);

	public EarthquakesAdapter(Context context, List<Earthquake> objects) {
		super(context, R.layout.cell_earthquake, objects);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder vh;
		Earthquake object = getItem(position);

		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(getContext());
			view = inflater.inflate(R.layout.cell_earthquake, null, true);
			vh = new ViewHolder(view);
			view.setTag(vh);
		} else
			vh = (ViewHolder) view.getTag();
		if (object == null)
			return view;

		vh.magnitudeLabel.setText(Double.toString(object.getMagnitude()));
		vh.placeLabel.setText(object.getPlace());
		vh.timeLabel.setText(dateFormat.format(object.getTime()) + " UTC");
        vh.magnitudeLabel.setBackgroundColor(getContext().getResources().getColor(object.getMagTypeColor()));

        if (object.isTsunami()) {
            vh.tsunamiIcon.setVisibility(View.VISIBLE);
        } else {
            vh.tsunamiIcon.setVisibility(View.GONE);
        }

		return view;
	}

	class ViewHolder {
		@InjectView(R.id.mag_label)
		TextView magnitudeLabel;
		@InjectView(R.id.place_label)
		TextView placeLabel;
		@InjectView(R.id.time_label)
		TextView timeLabel;
        @InjectView(R.id.tsunami_icon)
        ImageView tsunamiIcon;

		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
		}
	}

}
