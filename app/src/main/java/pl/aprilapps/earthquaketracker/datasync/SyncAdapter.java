package pl.aprilapps.earthquaketracker.datasync;

import java.util.List;

import pl.aprilapps.earthquaketracker.model.Earthquake;
import pl.aprilapps.earthquaketracker.requests.EarthquakesRequest;
import pl.aprilapps.earthquaketracker.utils.EqtDbUtils;
import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

/**
 * Created by jacek on 23.08.2014.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

	static final String LOG_TAG = SyncAdapter.class.getSimpleName();

	boolean downloading;

	public SyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
	}

	public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
		super(context, autoInitialize, allowParallelSyncs);
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
		if (!downloading) {
			try {
				Dao<Earthquake, String> earthquakeDao = EqtDbUtils.getDao(Earthquake.class, getContext());
				QueryBuilder<Earthquake, String> earthquakeQb = earthquakeDao.queryBuilder();

				EarthquakesRequest request = new EarthquakesRequest(getContext(), false);
				List<Earthquake> list = request.getResponseObject();

				for (Earthquake earthquake : list) {
					earthquakeQb.where().idEq(earthquake.getId());
					earthquakeDao.createOrUpdate(earthquake);
				}

				Log.i(LOG_TAG, "Data synced successfully");
			} catch (Exception e) {
				Log.e(LOG_TAG, "Data sync failed. " + e.getMessage());
			}

		}

	}
}
