package pl.aprilapps.earthquaketracker.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.fragments.EarthquakeDetailsInfoFragment;
import pl.aprilapps.earthquaketracker.fragments.FiltersFragment;
import pl.aprilapps.earthquaketracker.fragments.TweetsFragment;
import pl.aprilapps.earthquaketracker.model.Earthquake;

import static com.google.android.gms.common.GooglePlayServicesUtil.*;

/**
 * Created by jacek on 23.08.2014.
 */
public class EarthquakeDetailsActivity extends EqtActivity {

    Earthquake earthquake;

    TweetsFragment tweetsFragment;
    MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthquake_details);
        ButterKnife.inject(this);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        //loading tweets drawer
        tweetsFragment = (TweetsFragment) getFragmentManager().findFragmentById(R.id.tweets_drawer);
        tweetsFragment.setUp(R.id.tweets_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.setRetainInstance(true);

        if (savedInstanceState == null) {
            earthquake = (Earthquake) getIntent().getSerializableExtra(KEY_EARTHQUAKE);

            //configuring map
            LatLng latLng = new LatLng(earthquake.getLatitude(), earthquake.getLongitude());
            Marker newmarker = mapFragment.getMap().addMarker(new MarkerOptions().position(latLng).title(earthquake.getPlace()));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(2f).build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            mapFragment.getMap().moveCamera(cameraUpdate);
        } else {
            earthquake = (Earthquake) savedInstanceState.getSerializable(KEY_EARTHQUAKE);
        }

       if (savedInstanceState == null) {
            injectFragment(new EarthquakeDetailsInfoFragment(), R.id.info_container, ANIM_NONE, ANIM_NONE, ANIM_NONE, ANIM_NONE, false, false);
        }
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_EARTHQUAKE, earthquake);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_tweets:
                if (tweetsFragment.getDrawerLayout().isDrawerOpen(tweetsFragment.getView()))
                    tweetsFragment.getDrawerLayout().closeDrawer(tweetsFragment.getView());
                else
                    tweetsFragment.getDrawerLayout().openDrawer(tweetsFragment.getView());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public Earthquake getEarthquake() {
        return earthquake;
    }
}
