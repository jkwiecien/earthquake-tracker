package pl.aprilapps.earthquaketracker.activities;

import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.earthquaketracker.EqT;
import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.earthquaketracker.adapters.EarthquakesAdapter;
import pl.aprilapps.earthquaketracker.fragments.EarthquakesFragment;
import pl.aprilapps.earthquaketracker.fragments.FiltersFragment;
import pl.aprilapps.earthquaketracker.model.Earthquake;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pl.aprilapps.earthquaketracker.tasks.LoadEarthquakesTask;
import pl.aprilapps.library.utils.GAEResponseObject;

import static com.google.android.gms.common.GooglePlayServicesUtil.isGooglePlayServicesAvailable;

public class HomeActivity extends EqtActivity {

    FiltersFragment filtersFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		ButterKnife.inject(this);

        //loading filters drawer
        filtersFragment = (FiltersFragment) getFragmentManager().findFragmentById(R.id.filters_drawer);
        filtersFragment.setUp(R.id.filters_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));

        int gsmStatus = isGooglePlayServicesAvailable(this);
        if (gsmStatus != ConnectionResult.SUCCESS) {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(gsmStatus, this, REQ_GMS_UNAVAILABLE_DIALOG);
            dialog.show();
        } else if (savedInstanceState == null) {
            //injecting initial fragment
            injectFragment(new EarthquakesFragment(), R.id.container, ANIM_NONE, ANIM_NONE, ANIM_NONE, ANIM_NONE, false, false);

            //running sync
            getApp().createSyncAccount();
            ContentResolver.setSyncAutomatically(getApp().getSyncAccount(), getString(R.string.sync_provider_authority),true);
            ContentResolver.setIsSyncable(getApp().getSyncAccount(), getString(R.string.sync_provider_authority), 1);
            ContentResolver.addPeriodicSync(getApp().getSyncAccount(), getString(R.string.sync_provider_authority), new Bundle(), SYNC_INTERVAL);
        }
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_GMS_UNAVAILABLE_DIALOG) {
            finish();
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
        switch (id) {
            case R.id.action_filters:
                if (filtersFragment.getDrawerLayout().isDrawerOpen(filtersFragment.getView()))
                    filtersFragment.getDrawerLayout().closeDrawer(filtersFragment.getView());
                else
                    filtersFragment.getDrawerLayout().openDrawer(filtersFragment.getView());
                break;

            case R.id.action_reload:
                reloadEarthquakes();
                break;
        }
		return super.onOptionsItemSelected(item);
	}

    public void reloadEarthquakes() {
        boolean tsunami = filtersFragment.isTsunamiChecked();
        EarthquakesFragment fragment = (EarthquakesFragment) getFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.loadEarthquakes(tsunami);
        }
    }


    public boolean isTsunamiChecked() {
        return filtersFragment.isTsunamiChecked();
    }


}
