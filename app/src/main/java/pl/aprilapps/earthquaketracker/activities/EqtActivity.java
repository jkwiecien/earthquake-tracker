package pl.aprilapps.earthquaketracker.activities;

import android.view.View;

import pl.aprilapps.earthquaketracker.Constants;
import pl.aprilapps.earthquaketracker.EqT;
import pl.aprilapps.earthquaketracker.R;
import pl.aprilapps.library.ui.activities.AaActionBarActivity;

/**
 * Created by jacek on 23.08.2014.
 */
public class EqtActivity extends AaActionBarActivity implements Constants {

    @Override
    public EqT getApp() {
        return (EqT) super.getApp();
    }

    public void hideProgressLayoutProgressBar() {
        if (getProgressLayout() != null) {
            findViewById(R.id.progress_bar).setVisibility(View.INVISIBLE);
        }
    }

    public void showProgressLayoutProgressBar() {
        if (getProgressLayout() != null) {
            findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onHomePressed() {
        finish();
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
    }
}
