package pl.aprilapps.earthquaketracker.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.aprilapps.earthquaketracker.Constants;

/**
 * Created by jacek on 23.08.2014.
 */
public class EqtDatabaseHelper extends OrmLiteSqliteOpenHelper implements Constants {

    final String LOG_TAG = EqtDatabaseHelper.class.getName();

    final static int DATABASE_VERSION = 4;
    final static String DATABASE_NAME = "eqt.db";

    static EqtDatabaseHelper instance = null;

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        Log.i(LOG_TAG, "creating new database schema");
        try {
            // create tables for all persistable classes
            for (Class<?> dataClass : Configuration.PERSISTABLE_CLASSES) {
                TableUtils.createTable(connectionSource, dataClass);
            }
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,  int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            recreateDatabase(db, connectionSource);
        }
    }

    private EqtDatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
    }

    public static EqtDatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new EqtDatabaseHelper(context, DATABASE_NAME);
        }
        return instance;
    }

    void recreateDatabase(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(LOG_TAG, "dropping database schema");
            // drop all tables
            for (Class<?> dataClass : Configuration.PERSISTABLE_CLASSES) {
                TableUtils.dropTable(connectionSource, dataClass, true);
            }
            // recreate database from scratch
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't drop database", e);
            throw new RuntimeException(e);
        }
    }
}
