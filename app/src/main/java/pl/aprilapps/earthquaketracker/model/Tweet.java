package pl.aprilapps.earthquaketracker.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import pl.aprilapps.earthquaketracker.Contract;
import twitter4j.Status;

/**
 * Created by jacek on 23.08.2014.
 */

@DatabaseTable(tableName = Contract.Tables.TWEETS)
public class Tweet extends AbstractBean {

    @DatabaseField(columnName = Contract.Tweet.TEXT)
    String text;

    @DatabaseField(columnName = Contract.Tweet.SOURCE)
    String source;

    @DatabaseField(columnName = Contract.Tweet.USER)
    String user;

    @DatabaseField(columnName = Contract.Tweet.PLACE)
    String place;

    @DatabaseField(columnName = Contract.Tweet.FAVOURITE_COUNT)
    int favoriteCount;

    public Tweet() {
        super();
    }

    public Tweet(Status status) {
        id = Long.toString(status.getId());
        createdAt = status.getCreatedAt();
        updated = status.getCreatedAt();
        text = status.getText();
        source = status.getSource();
        user = status.getUser() != null ? status.getUser().getScreenName() : null;
        place = status.getPlace() != null ? status.getPlace().getName() : null;
        favoriteCount = status.getFavoriteCount();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
    }
}
