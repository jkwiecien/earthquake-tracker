package pl.aprilapps.earthquaketracker.model;

import java.io.Serializable;
import java.util.Date;

import pl.aprilapps.earthquaketracker.Contract;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by jacek on 23.08.2014.
 */

@DatabaseTable
public class AbstractBean implements Serializable {

	@DatabaseField(columnName = Contract.Earthquake.ID, canBeNull = false, id = true)
	String id;

	@DatabaseField(columnName = Contract.Common.CREATED_AT, canBeNull = false, dataType = DataType.DATE_LONG)
	protected Date createdAt;

	@DatabaseField(columnName = Contract.Common.ACTIVE)
	protected boolean active;

    @DatabaseField(columnName = Contract.Common.UPDATED, dataType = DataType.DATE_LONG)
    Date updated;

	@DatabaseField(columnName = Contract.Common.INTERNAL_ID, canBeNull = false, defaultValue = "1")
	protected Long internalID;

	public AbstractBean() {
		active = true;
		createdAt = new Date();
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getInternalID() {
		return internalID;
	}

	public void setInternalID(Long internalID) {
		this.internalID = internalID;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
