package pl.aprilapps.earthquaketracker.model;

import java.util.LinkedList;
import java.util.List;

import android.util.Log;

import com.j256.ormlite.table.DatabaseTable;

/**
 * Class to contain static fields that need to be explicitly initialized and thus cannot be put in an interface.
 * Created by jacek on 23.08.2014.
 */
public class Configuration {

	// all tables that need persisting in local db
	public static final List<Class<? extends AbstractBean>> PERSISTABLE_CLASSES = new LinkedList<Class<? extends AbstractBean>>();

	static {
		addPersistableClass(Earthquake.class);
        addPersistableClass(Tweet.class);
	}

	private static void addPersistableClass(Class<? extends AbstractBean> persistableClass) {
        String tableName = persistableClass.getAnnotation(DatabaseTable.class).tableName();
        PERSISTABLE_CLASSES.add(persistableClass);
    }
}
