package pl.aprilapps.earthquaketracker.model;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import pl.aprilapps.earthquaketracker.Contract;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by jacek on 23.08.2014.
 */

@DatabaseTable(tableName = Contract.Tables.EARTHQUAKES)
public class Earthquake extends AbstractBean {

	@DatabaseField(columnName = Contract.Earthquake.MAGNITUDE, canBeNull = false)
	double magnitude;
	@DatabaseField(columnName = Contract.Earthquake.PLACE)
	String place;
	@DatabaseField(columnName = Contract.Earthquake.TIME, dataType = DataType.DATE_LONG)
	Date time;
	@DatabaseField(columnName = Contract.Earthquake.URL)
	String url;
	@DatabaseField(columnName = Contract.Earthquake.STATUS)
	String status;
	@DatabaseField(columnName = Contract.Earthquake.TSUNAMI)
	boolean tsunami;
	@DatabaseField(columnName = Contract.Earthquake.MAGNITUDE_TYPE)
	String magnitudeType;
	@DatabaseField(columnName = Contract.Earthquake.LONGITUDE)
	double longitude;
	@DatabaseField(columnName = Contract.Earthquake.LATITUDE)
	double latitude;
	@DatabaseField(columnName = Contract.Earthquake.DEPTH)
	double depth;

	public Earthquake() {
		super();
	}

	public Earthquake(JSONObject json) {
		id = json.optString("id");

		JSONObject propertiesJson = json.optJSONObject("properties");
		magnitude = propertiesJson.optDouble("mag");
		place = propertiesJson.optString("place");
		time = new Date(propertiesJson.optLong("time"));
		updated = new Date(propertiesJson.optLong("updated"));
		url = propertiesJson.optString("url");
		status = propertiesJson.optString("status");

		String tsunamiString = propertiesJson.optString("tsunami");
		if (!tsunamiString.equals("null")) {
			tsunami = true;
		}

		magnitudeType = propertiesJson.optString("magnitudeType");

		JSONArray coordinatesJsonArray = json.optJSONObject("geometry").optJSONArray("coordinates");
		longitude = coordinatesJsonArray.optDouble(0);
		latitude = coordinatesJsonArray.optDouble(1);
		depth = coordinatesJsonArray.optDouble(2);
	}

	public double getMagnitude() {
		return magnitude;
	}

	public String getPlace() {
		return place;
	}

	public Date getTime() {
		return time;
	}

	public String getUrl() {
		return url;
	}

	public String getStatus() {
		return status;
	}

	public boolean isTsunami() {
		return tsunami;
	}

	public String getMagnitudeType() {
		return magnitudeType;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getDepth() {
		return depth;
	}

	public void setMagnitude(double magnitude) {
		this.magnitude = magnitude;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTsunami(boolean tsunami) {
		this.tsunami = tsunami;
	}

	public void setMagnitudeType(String magnitudeType) {
		this.magnitudeType = magnitudeType;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public int getMagTypeColor() {
		if (magnitudeType.equalsIgnoreCase("mw")) {
			return android.R.color.holo_blue_light;
		} else if (magnitudeType.equalsIgnoreCase("mb")) {
			return android.R.color.holo_blue_dark;
		} else if (magnitudeType.equalsIgnoreCase("ms")) {
			return android.R.color.holo_orange_light;
		} else if (magnitudeType.equalsIgnoreCase("me")) {
			return android.R.color.holo_orange_dark;
		} else if (magnitudeType.equalsIgnoreCase("ml")) {
			return android.R.color.holo_purple;
		} else if (magnitudeType.equalsIgnoreCase("md") || magnitudeType.equalsIgnoreCase("cl")) {
			return android.R.color.holo_red_light;
		} else if (magnitudeType.equalsIgnoreCase("fa") || magnitudeType.equalsIgnoreCase("mi")) {
			return android.R.color.holo_green_light;
		} else {
			return android.R.color.darker_gray;
		}
	}
}
